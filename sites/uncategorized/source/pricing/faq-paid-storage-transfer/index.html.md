---
layout: markdown_page
title: Storage and Transfer limits
description: "On this page you can view frequently asked questions for storage and transfer limits for the paid GitLab SaaS tiers"
canonical_path: "/pricing/faq-paid-storage-transfer/"
---

# Frequently Asked Questions

{:.no_toc}

### On this page

{:.no_toc}

{:toc}

- TOC

## GitLab SaaS Premium and Ultimate

**Q. What is changing with Storage and Transfer limits?**  
A. We are introducing storage and transfer limits to the [GitLab SaaS offerings](https://about.gitlab.com/pricing).

| GitLab SaaS Tier                         | Free | Premium | Ultimate |
|------------------------------------------|:----:|:-------:|:--------:|
| Price                                    | $0   | $19     | $99      |
| Storage (in GB per namespace)            | 5 GB  | 50 GB  | 250 GB   |
| Transfer (in GB per namespace per month) | 10 GB | 100 GB | 500 GB   |

**Q. Which users are these changes applicable to?**  
A. These limits are applicable to users of the GitLab SaaS offerings. These changes do not apply to self-managed users (both free and paid tier) and community programs - including GitLab for Open Source, Education and Startups users.

Customers who are still on the Bronze subscription are not impacted by this change immediately, and the new limits on Premium / Ultimate will be applicable when they upgrade.

**Q. Do these changes apply to Trials?**  
A. Yes, GitLab Trial will receive entitlements from GitLab Ultimate.

**Q. What is the effective date of the changes?**  
A. For existing paid tier users:

- These limits will not apply immediately and will be applicable to your subscription at your next renewal on or after **2022-10-15**.
- Storage limits will be enforced for applicable subscriptions from **2023-02-15**.
- Transfer limits will not be enforced until further communication from GitLab.

**Q. Will enforcement start when both storage and transfer are available?**  
A. No, enforcement of storage and transfer will be independent of each other. Storage usage visibility and enforcement will be first, transfer usage visibility and enforcement will follow.

### Managing your Storage Usage

**Q. What constitutes Storage usage?**  
A. Currently storage includes [artifacts](https://docs.gitlab.com/ee/ci/pipelines/pipeline_artifacts.html#storage), [repositories](https://docs.gitlab.com/ee/user/project/repository/#repository), [package](https://docs.gitlab.com/ee/user/packages/package_registry/) and [container](https://docs.gitlab.com/ee/user/packages/container_registry/) registries, [snippets](https://docs.gitlab.com/ee/user/snippets.html#snippets), [LFS](https://docs.gitlab.com/ee/topics/git/lfs/#git-large-file-storage-lfs), [wiki](https://docs.gitlab.com/ee/user/project/wiki/#wiki) storage, [dependency proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/). Future product features that allow you to store data will also be incorporated into the storage usage count, when available.

**Q. How can I view and manage my storage usage?**  
A. You can view artifacts, repositories, package registries, snippets, packages, uploads, LFS and wiki storage on the Group Settings `Usage Quota` Page.

**Q. How can I reduce the amount of Storage consumed?**  
A. Below you will find steps for managing artifacts, repositories, snippets, packages and wikis.

- Artifacts: You can set an [expiration policy](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#when-job-artifacts-are-deleted) for when artifacts can be deleted. You can also [delete job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#delete-job-artifacts) manually - remember this can lead to data loss.
- Repositories: Repositories become large over time. You can reduce your repository size using the steps highlighted [here](https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html).
- Container Registry: You can set an [expiration policy](https://docs.gitlab.com/ee/user/packages/container_registry/reduce_container_registry_storage.html#cleanup-policy) to programmatically remove old, unused images and tags. You can also use the [user interface or API](https://docs.gitlab.com/ee/user/packages/container_registry/reduce_container_registry_storage.html)to delete items as well.
- Snippets: Snippets follow the same process as [reducing repository size](https://docs.gitlab.com/ee/user/snippets.html#reduce-snippets-repository-size).
- Packages: You can delete an [entire package](https://docs.gitlab.com/ee/user/packages/package_registry/reduce_package_registry_storage.html#delete-a-package) or [files within a package](https://docs.gitlab.com/ee/user/packages/package_registry/reduce_package_registry_storage.html#delete-files-associated-with-a-package)
- Dependency Proxy: You can set an [expiration policy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/reduce_dependency_proxy_storage.html#cleanup-policies) to programmatically clear the cache. Or, you can manually [purge the cache](https://docs.gitlab.com/ee/user/packages/dependency_proxy/reduce_dependency_proxy_storage.html#use-the-api-to-clear-the-cache) using the API.
- Wikis: Wikis follow the same process as [reducing repository size](https://docs.gitlab.com/ee/administration/wikis/index.html#reduce-wiki-repository-size).

**Q. How will the storage limits affect me when I’m contributing to GitLab?**  
A. Forks of GitLab get deduplicated, so only the changes you make will contribute to your storage consumption for this fork.

### Managing your Transfer Usage

**Q. What constitutes transfer usage?**  
A. Transfer is measured as the amount of data egress leaving GitLab.com, except for:

- Transfer generated by GitLab.com [Shared Runners](https://docs.gitlab.com/ee/ci/runners/)
- Transfer generated by GitLab Runners authenticating with a `CI_JOB_TOKEN`
- Transfer generated by deployments authenticating with a `DEPLOY_TOKEN`
- The web interface

The primary use cases affected by transfer limits are end users working with code repositories, and public distribution of containers and packages.
Future product features that allow transfer of data will also be incorporated into the transfer usage count, when available.

**Q. Can I proactively monitor and reduce my transfer usage?**  
A. Viewing transfer usage is not yet available. Until the usage report is available on the Group Settings Usage `Quota Page`, transfer limits will not be programmatically enforced. If your usage is significantly high, the GitLab team will reach out to you to help manage your usage.

### Purchasing additional Storage and Transfer

**Q. How much does it cost to buy additional Storage and Transfer units?**  
A. Additional storage or transfer units can be purchased from the [GitLab Customer Portal](https://customers.gitlab.com/) at $60/year that includes both 10GB of storage and 20GB/month transfer.
For example:

- If you require 15GB storage, you will pay $120 for the year.
- If you require 25GB transfer per month, you will pay $120 for the year.
- If you require both 15GB storage and $25GB Transfer per month, you will still pay $120 for the year.
- If you have purchased 5 add-ons, you will be entitled to 50GB storage and 100GB transfer/month.

### More information

For more information on managing and reducing your storage usage, please contact GitLab support or your GitLab sales representative.
